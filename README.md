# LulzSec Email Sorter

After [LulzSec](http://en.wikipedia.org/wiki/LulzSec) posted some interesting files on [Pastebin.com](https://pastebin.com/Fjg6Gs7y) in 2012, I became especially interested in a file which contained thousands of email addresses they swiped from a database. This recipe determines the most frequent domain names and most frequent usernames by writing to two separate text files (also removing duplicates and sorting in alphabetical order). Requires Python 3.

## TL;DR

Top 30 most common domains:
* hotmail.com, 27469 
* yahoo.com, 26844 
* aol.com, 16923 
* gmail.com, 11741 
* comcast.net, 9544 
* msn.com, 5606 
* sbcglobal.net, 2814 
* earthlink.net, 2634 
* shaw.ca, 2552 
* verizon.net, 2264 
* charter.net, 1491 
* sympatico.ca, 1340 
* juno.com, 1335 
* telus.net, 1310 
* cox.net, 1266 
* mac.com, 1085 
* rogers.com, 1011 
* bellsouth.net, 925 
* yahoo.ca, 911 
* adelphia.net, 806 
* att.net, 741 
* yahoo.co.uk, 708 
* optonline.net, 688 
* gci.net, 571 
* AOL.COM, 567 
* excite.com, 516 
* videotron.ca, 504 
* netscape.net, 473 
* btinternet.com, 467 
* mindspring.com, 443

Top 30 most common usernames:
* info, 191 
* smith, 104 
* 1, 97 
* apollo, 73 
* mike, 72 
* apollohealth, 71 
* john, 70 
* david, 63 
* mail, 61 
* chris, 57 
* m, 55 
* mark, 54 
* steve, 53 
* brown, 51 
* j, 49 
* scott, 48 
* miller, 46 
* julie, 45 
* lee, 44 
* lisa, 43 
* linda, 43 
* paul, 43 
* golite, 42 
* karen, 42 
* susan, 42 
* b, 40 
* johnson, 40 
* a, 39 
* sue, 38 
* bob, 38