# Date: 11/6/12

#!/usr/bin/env python
import re, os.path, collections

# open files for reading and writing
f_email_db = open("emails_db.txt", "r")
f_usernames = open("usernames.txt", 'w')
f_domains = open("domains.txt", 'w')

# instantiate empty lists 
usernames_list = []
domains_list = []

# compile regex for efficient searching
regex = re.compile(r'([\w]+)@([\w.]+)')

# loop through email text file and append the regex of the first
# set of parentheses to the username list, and append the regex
# of the second set of parentheses to the domain list
for email in f_email_db:
	match = regex.search(email)
	usernames_list.append(match.group(1))
	domains_list.append(match.group(2)) 

print("Email username list from hacked Philips database (Lulzsec link: http://pastebin.com/Fjg6Gs7y):", file=f_usernames)
print("Top 30 most common usernames in the dump:", file=f_usernames)

# instantiate a new counter to store most common usernames
username_count = collections.Counter(usernames_list).most_common(30)
print(username_count, file=f_usernames)

print("Email domain list from hacked Philips database (Lulzsec link: http://pastebin.com/Fjg6Gs7y):", file=f_domains)
print("Top 30 most common domains in the dump:", file=f_domains)

# instantiate a new counter to store most common domain names
domain_count = collections.Counter(domains_list).most_common(30)
print(domain_count, file=f_domains)

# remove duplicates in lists
usernames_list = list(set(usernames_list))
domains_list = list(set(domains_list))

# sort both lists in numerical and alphabetical order
usernames_list.sort()
domains_list.sort()

print(file=f_usernames) # print new line
print("All usernames in sorted numerical and then alphabetical order (with duplicates removed):", file=f_usernames)

# print each element in usernames list to file
for name in usernames_list:
	print(name, file=f_usernames)

print(file=f_domains) # print new line
print("All domains in sorted numerical and then alphabetical order (with duplicates removed):", file=f_domains)

# print each element in domains list to file
for domain in domains_list:
	print(domain, file=f_domains)

# close files
f_domains.close()
f_usernames.close()
f_email_db.close()
